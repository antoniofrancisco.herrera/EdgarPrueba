package prueba;

import org.testng.annotations.Test;

public class PruebaJenkins2
{
	
	@Test
    public void test1()
    {
        System.out.println( "Hola Jenkins. Te estoy probando en una ejecucion." );
    }
	
	@Test
    public void test2()
    {
        System.out.println( "Hola Jenkins. Te estoy probando en una ejecucion x2" );
    }
	
	@Test
    public void test3()
    {
        System.out.println( "Hola Jenkins. Te estoy probando en una ejecucion x3" );
    }
	
	@Test
    public void test4()
    {
        System.out.println( "Hola Jenkins. Te estoy probando en una ejecucion x4" );
    }
	
	@Test
    public void test5()
    {
        System.out.println( "Hola Jenkins. Te estoy probando en una ejecucion x5" );
    }
	
	@Test
    public void test6()
    {
        System.out.println( "Hola Jenkins. Te estoy probando en una ejecucion x6" );
    }

}